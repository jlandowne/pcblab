/*
 * MorseTransmitter.h
 * ------------------
 * Public function declarations as well as the sentence to be transmitted
 * are stored in this file.  Compiles with PICC-LITE 9.60PL2 and above.
 * 
 * Revision History
 * ----------------
 * 2008.10.11 |   KLG & SJL   |   Added MAX_REPEATS #define
 * 2008.9.1   |   KLG & SJL   |   Initial version
 *
 */

#ifndef MORSETRANSMITTER_H_
#define MORSETRANSMITTER_H_

/*
 * MAX_REPEATS
 * -----
 * Defines the number of times the message will repeat before the
 * program times out and shuts down the CPU.
 * Permitted values: 0-255.
 */
#define MAX_REPEATS 20

/*
 * IRLED
 * -----
 * IRLED is the port bit that the transmitting LED is connected to.  It
 * must be on when the port is hi, and off when the port is low. The
 * proper DDR setting must be done elsewhere in the code.
 */
#define IRLED LATAbits.LATA4//Defining LED pin

/*
 * Morse Sentence Instructions:
 * ----------------------------
 * 1. First and last characters cannot be spaces, spaces must not be adjacent
 *    to other spaces!
 * 2. TYPE IN ALL CAPS!
 * 3. The following non-letter, non-numeral characters are acceptable:
 *      .,?'!/():;=+-_"$&@
 * 	  remember to escape those characters that need to be escaped.
 * 4. Any illegal character will be transmitted as an error (8 dots)
 * 5. The max sentence length (including spaces) is 729 characters.
 * 6. Behavior of the MorseTransmitter module if you do not follow the above
 *    rules is UNDEFINED and AT YOUR OWN RISK. You might even esssssplode
 *    the earth.
 */
//#define MORSE_SENTENCE "TONIGHT'S WEATHER FORECAST: THERE'S GOING TO BE A DARK KNIGHT."
#define MORSE_SENTENCE "ED SPEAKS MANY LANGUAGES, SUCH AS ASSEMBLY AND HEX."

/*
 * MorseInit
 * ---------
 * Call this function once to initialize the MorseTransmitter module
 * and prepare for transmission.
 */
void MorseInit(void);

/*
 * MorseDisplay
 * ------------
 * The morse transmission function.  This needs to be called repeatedly
 * ad infinitum, with some kind of pause inbetween calls.  The pause should
 * represent one dot-time.  It is theoretically possible to call it in rapid
 * succession, but then the timing of all of its internal processes will
 * dominate, and sensical morse output is not guaranteed.
 */
void MorseDisplay(void);


#endif /* MORSETRANSMITTER_H_ */