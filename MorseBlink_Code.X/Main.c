/*
 * MorseTransmitter / main.c
 * -------------------------
 * Contains the config word for the PIC12F629 and the relevant calls to Morse
 * functions. Also contains all hardware-relevant initializations.
 * Compiles with PICC-LITE 9.60PL2 and above.
 * 
 * Revision History
 * ----------------
 * 2008.10.11 |   KLG & SJL   |   Renamed Sleep to avoid function overload
 * 2008.9.1   |   KLG & SJL   |   Initial version
 *
 */

#include <htc.h>
#include "MorseTransmitter.h"
#include <stdint.h>

/* Program device configuration word
 * Oscillator = Internal RC No Clock
 * Watchdog Timer = On
 * Power Up Timer = Off
 * Master Clear Enable = External
 * Brown Out Detect = On
 * Code Protect = Off
 * Data EE Read Protect = Off
 * Bandgap Calibration Bits = Highest Bandgap Voltage
 */
// __config(_CP_OFF & _WDTE_OFF & _PWRTE_ON & _FOSC0_INT);    


/*** Function Declarations ***/
void Initialize(void);  // Initializes! What else?
static void EnterSleep(void);  // Calls the asm sleep instruction
static void SetInitialADCVal(void); //Sets base threshold for Temp. ADC value
static char GetADVal(void); //Gets ADC Val from Temp Sensor

static char BaseVal; //Base Threshold for Temp. value

/*** main ***/
void main(void)
{
	Initialize(); // Initialize processor and timing quirks
	MorseInit();  // Initialize MorseTransmitter module
	
	while (1){
		MorseDisplay();  // Transmit the relevant morse message
		EnterSleep();
	}
}


void Initialize(void)
{
	/*** Oscillator Calibration ***/
	// This macro runs the last instruction in program memory,
	// which is factory-calibrated to write the tuned internal
	// oscillator frequency to the proper register.
	//OSCCAL = _READ_OSCCAL_DATA();
	OSCCONbits.IRCF = 0x11; // 8 MHZ oscillator
    
	/*** Interupts ***/
	// Disable all interrupts by clearing the INTCON register.
	//INTCON	= 0b00000000;

    HLTPR1 = 3; //Baud Rate Value for HLTMR1 -- 5us
    HLT1CON0bits.H1OUTPS = 0x01; //Post scale 2, prescale 1
    HLT1CON1 =  0b00011000; //Reserved input source, no effect of RE or FE
    INTCONbits.GIE = 1; //global int set
    INTCONbits.PEIE = 1;  //peripheral int set
    PIE1bits.HLTMR1IE = 1;  //Enables Hardware timer int
	/*** Ports ***/
	// 0 = output, 1 = input
	// GP0 = unused (input)
	// GP1 = comparator input (input)
	// GP2 = comparator output (output)
	// GP3 = MCLR (input by default)
	// GP4 = IR LED output (output)
	// GP5 = unused (input)
	TRISA	= 0b00101011;
    //Setting Analog Port for A/D Converter
    // GP4 = Analog 
    // Everything else is digital
    ANSELA = 0b0001000;
	
	/*** Watchdog Timer ***/
	// The following 4 bits are all in the OPTION_REG register
	// and control the Watchdog Timer prescaler.
	OPTION_REGbits.PSA = 1;  // Assign prescaler to the WDT
	OPTION_REGbits.PS2 = 0;  //
	OPTION_REGbits.PS1 = 1;  // Set prescaler to 1:8 (nominal period 8*18ms=144ms)
	OPTION_REGbits.PS0 = 1;  // 
	
//	/*** Internal Voltage Reference for Comparator ***/
//	// We're using the internal voltage reference, which eats some
//	// power during sleep, but saves us the trouble of having an
//	// external voltage divider. We're setting it to the low range
//	// which gives us a Vref = (VR3:VR0 / 24) * VDD.
//	VRR = 1;  // Set to low range
//	VR3 = 0;  //
//	VR2 = 1;  // VR3:VR0 is 0111 = 7, so with a VDD of 3.3V we get:
//	VR1 = 1;  // Vref = (7/24)*3.3 = 0.9625V.
//	VR0 = 1;  //
////	VREN = 1; // Switch on the Vref circuits
////	
//	/*** Comparator ***/
//	// We set up the comparator to use the inverting input pin
//	// and the internal voltage reference.
//	CM2 = 0;  //
//	CM1 = 1;  // Mode select bits (see datasheet (DS41190E) p.37)
//	CM0 = 1;  //
//	CINV = 1; // Invert output (we want pin high -> output high)
}

static void SetInitialADCVal(void) {
    long Sum;
    
    for(char i=0; i<4; i++) {
        Sum += GetADVal(); //Gets sum of 4 AD values
    }
    
    BaseVal = Sum >>2;
    
    
}

static char GetADVal(void) {
    return 1;
}

static void EnterSleep(void)
{
	asm("sleep");	// PICASM sleep!
}