
/*
 * MorseTransmitter.c
 * ------------------
 * All of the MorseTransmitter nitty-gritty is in here.  Don't look, you might
 * be frightened.  Compiles with PICC-LITE 9.60PL2 and above.
 * 
 * Revision History
 * ----------------
 * 2008.10.11 |   KLG & SJL   |   Added Timeout After # of Repeats
 * 2008.9.1   |   KLG & SJL   |   Initial version
 *
 */

#include "MorseTransmitter.h"
#include <htc.h>

#ifndef MORSETRANSMITTER_C_
#define MORSETRANSMITTER_C_


/*** Private Constants ***/
/*** Morse Lookup Table ***/
// This lookup table stores dot-dash sequences for every ASCII
// character from 0x21 (exclamation mark) to 0x5F (underscore).
// In case a particular character has no morse transliteration,
// then the error code (8 dots) is stored. A dash is represented
// by a 1, a dot by a 0, from LSB+length to LSB. length is read
// in from MorseTableLengths, which shares the same index as
// MorseTable. So for instance, if you were reading in 'A', which
// is ASCII 0x41, you'd first subtract 0x21 and use that as an
// index in MorseTable. This gets you 0b00000001. Then you get the
// length from MorseTableLengths, which is 2. Then you know 'A'
// is represented by the least significant 2 bits, or 01; dot-dash.
static const unsigned char MorseTable[] =
{
	0b00101011, // ! -.-.--
	0b00010010, // " .-..-.
	
	0b00000000, // error
	0b00001001, // $ ...-..-
	0b00000000, // error
	0b00001000, // & .-...
	0b00011110, // ' .----.
	0b00010110, // ( -.--.
	0b00101101, // ) -.--.-
	
	0b00000000, // error
	0b00001010, // + .-.-.
	0b00110011, // , --..-
	0b00100001, // - -....-
	0b00010101, // . .-.-.-
	0b00010010, // / -..-.
	0b00011111, // 0 -----
	
	0b00001111, // 1 .----
	0b00000111, // 2 ..---
	0b00000011, // 3 ...--
	0b00000001, // 4 ....-
	0b00000000, // 5 .....
	0b00010000, // 6 -....
	0b00011000, // 7 --...
	
	0b00011100, // 8 ---..
	0b00011110, // 9 ----.
	0b00111000, // : ---...
	0b00101010, // ; -.-.-.
	0b00000000, // error
	0b00010001, // = -...-
	0b00000000, // error
	
	0b00001100, // ? ..--..
	0b00011010, // @ .--.-.
	0b00000001, // A .-
	0b00001000, // B -...
	0b00001010, // C -.-.
	0b00000100, // D -..
	0b00000000, // E .
	
	0b00000010, // F ..-.
	0b00000110, // G --.
	0b00000000, // H ....
	0b00000000, // I ..
	0b00000111, // J .---
	0b00000101, // K -.-
	0b00000100, // L .-..
	
	0b00000011, // M --
	0b00000010, // N -.
	0b00000111, // O ---
	0b00000110, // P .--.
	0b00001101, // Q --.-
	0b00000010, // R .-.
	0b00000000, // S ...
	
	0b00000001, // T -
	0b00000001, // U ..-
	0b00000001, // V ...-
	0b00000011, // W .--
	0b00001001, // X -..-
	0b00001011, // Y -.--
	0b00001100, // Z --..
	
	0b00000000, // error
	0b00000000, // error
	0b00000000, // error
	0b00000000, // error
	0b00001101, // _ ..--.-
};

static const unsigned char MorseTableLengths[] =
{
	6,6,            // !,$
	8,7,8,5,6,5,6,  // err,$,err,&,',(,)
	8,5,6,6,6,5,5,  // err,+,,,-,.,/,0
	5,5,5,5,5,5,5,  // 1,2,3,4,5,6,7
	5,5,6,6,8,5,8,  // 8,9,:,;,err,=,err
	6,6,2,4,4,3,1,  // ?,@,A,B,C,D,E
	4,3,4,2,4,3,4,  // F,G,H,I,J,K,L
	2,2,3,4,4,3,3,  // M,N,O,P,Q,R,S
	1,3,4,3,4,4,4,  // T,U,V,W,X,Y,Z
	8,8,8,8,6		// err,err,err,err,_
};

static const unsigned char Sentence[] = MORSE_SENTENCE; // #defined in MorseTransmitter.h


/*** Module Variables ***/
static unsigned char SentenceIndex;    // Stores the current (letter) position in the sentence
static unsigned char MorseArray;       // Stores the dot-dash sequence of the letter being transmitted
static unsigned char MorseArrayLength; // Stores the length of the above sequence
static unsigned char MorseArrayIndex;  // Stores the position in the above sequence
static unsigned char SleepCycles;      // Stores a number of cycles relevant to dash/space/etc. timing
static unsigned char NumRepeats;       // Stores the number of times the sentence has been repeated
static unsigned char SleepMode;        // 1 = program is in sleep mode, 0 = not in sleep mode


/*** Private Function Declarations ***/
static void MorseTableLookup(unsigned char Letter);


/*** Function Definitions***/

void MorseInit(void)
{	
	SentenceIndex = 0;
	MorseArray = 0;
	MorseArrayLength = 0;
	MorseArrayIndex = 0;
	SleepCycles = 0;
	NumRepeats = 0;
	SleepMode = 0;

	// Load first character of string
	MorseTableLookup(Sentence[SentenceIndex]);	
}

void MorseDisplay(void)
{
	// MorseDisplay relies one some pretty complex process design.
	// The idea is that it's only called every dot time, when there
	// is potential for an edge in the morse signal. Therefore, the
	// code is all designed to check what kind of edge we need to 
	// output, if any.
	
	// Check if in sleep mode - if so, return
	if (SleepMode)
	{
		return;
	}
	
	// Check SleepCycles - if this is positive, then we're in
	// the middle of a longer transmission and no edge is occurring
	// in this call of MorseDisplay.
	if (SleepCycles != 0)
	{
		SleepCycles--;  // Decrement the SleepCycles count
	}
	
	// Otherwise, we're ready to place an edge. What kind of edge
	// we place depends on the current status of the IRLED.
	else
	{
		// Check the status of the IRLED
		// If IRLED is on (currently transmitting a dot or dash)
		if (IRLED)
		{
			IRLED = 0;  // Turn the IRLED off
		}
		
		// Else IRLED is off (currently transmitting a break of some sort)
		else
		{
			// Check if we need to load the next letter. We do this
			// By looking if the MorseArrayIndex has been decremented
			// past 0 with the last MorseDisplay call.
			if(MorseArrayIndex == 255)
			{
				SleepCycles = 1;  // This is the timing necessary to insert
				                  // a character space (3 dots wide). Because
				                  // of the way this function works, it's a 
				                  // different number from timing for a dash.

				// Time to load the next letter in the sentence!
				// First, we increment the SentenceIndex
				SentenceIndex++;
				
				// Then, check for null character (end of sentence)
				if(Sentence[SentenceIndex] == '\0')
				{
					// End of sentence!
					// Reset SentenceIndex back to 0
					// ("next letter" actually loops to the first)
					SentenceIndex = 0;
					// Set SleepCycles to 5 for a word space (7 dots)
					SleepCycles = 5; 
					// Increment NumRepeats
					NumRepeats++;
					// Check for MAX_REPEATS, if we reach it then shut off the WDT
					// thereby preventing the processor from waking from sleep
					if(NumRepeats == MAX_REPEATS)
					{
						// Go into sleep mode - can only wake by MCLR
						SleepMode = 1;
						IRLED = 0;
					}
				}
				// If we're not at the end of the sentence, then look
				// for the space character
				else if(Sentence[SentenceIndex] == ' ') // space within sentence
				{
					// Increment to the *next* letter (after the space)
					// So that the table lookup doesn't try to find a space.
					SentenceIndex++;
					// Set SleepCycles to 5 for a word space (7 dots)
					SleepCycles = 5;
					
				}
				
				// Load the next letter in the sentence as defined by the
				// current value of SentenceIndex
				MorseTableLookup(Sentence[SentenceIndex]);
			}
			
			// Else, we continue transmitting the dots and dashes
			// of the current letter in the sentence.
			else
			{
				// Check if bit at index location is 1 (check if dash)
				if((MorseArray >> MorseArrayIndex) & (0b00000001))
				{
					SleepCycles = 2;  // Add some timing if it's a dash
				}
				
				// Turn IRLED on
				IRLED = 1;
				MorseArrayIndex--;  // Decrement in preparation for next call
			}
		}
	}
}

static void MorseTableLookup(unsigned char Letter)
{
	// Check if letter is within valid Morse Table bounds
	if((Letter > 32) && (Letter < 96))
	{
		Letter  = Letter - 33;  // Obtain relative index value for this range
		MorseArray = MorseTable[Letter];  // Match letter to morse representation
		MorseArrayLength = MorseTableLengths[Letter];  // Find length of morse representation
	}
	else  // Letter is not within valid Morse Table bounds, so throw error
	{
		MorseArray = 0b00000000;
		MorseArrayLength = 8;
	}
	
	// Lastly, reset the MorseArrayIndex (remember it's read left to right so the index
	// starts at length-1)
	MorseArrayIndex = MorseArrayLength - 1;
}


#endif /* MORSETRANSMITTER_C_ */